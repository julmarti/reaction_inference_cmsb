import copy
from itertools import combinations

from data_processing import filter_var, trace_finder

import numpy as np


def mal_inference(predecessors, dt, diff, reaction, sp, cata=[]):
    """Compute mal parameter given transitions, predecessor states and index of choosen species to compute variations (proper to each reaction).

    (Algo 5 in article)
    """
    reactants, products = list(np.where(reaction == -1)[0]), list(np.where(reaction == 1)[0])
    K, s_involved = [], reactants + products
    reactants = reactants + cata
    count = 0
    for p in range(len(predecessors)):
        # avoid division by zero and numerical discrepancies
        if (np.prod([predecessors[p][r + 1] for r in reactants]) > 0) and (np.prod([diff[p, s] for s in s_involved])):
            count += 1
            # case x => _
            if not np.where(reaction == 1)[0].size:
                k = -diff[p, sp] / (dt[p] * np.prod([predecessors[p][r + 1] for r in reactants]))
            # case _ => x
            if not np.where(reaction == -1)[0].size:
                if not len(cata):
                    k = diff[p, sp] / dt[p]
                else:
                    k = diff[p, sp] / (dt[p] * predecessors[p][cata[0] + 1])
            else:
                k = diff[p, sp] / (dt[p] * np.prod([predecessors[p][r + 1] for r in reactants]))
        else:
            continue
        K.append(k**(-1))
    avg_k = np.abs(1 / np.mean(K))
    return avg_k, np.std(K) * avg_k, count


def hill_inference(predecessors, dt, diff, reaction):
    """Compute hill order 1 and 4 kinetics given transitions and predecessor states. Only for single reactant reactions (Algo 6 in article)."""
    reactant, s_involved = np.where(reaction == -1)[0][0], np.where(reaction != 0)[0]
    V_1, V_4, gaps = [], [], []
    s_argmax = np.argmax([predecessors[i][reactant + 1] if diff[i, reactant] != 0 else 0 for i in range(len(predecessors))], axis=0)
    vmax = np.abs(diff[s_argmax, reactant] / dt[s_argmax])
    for d in range(len(diff)):
        gap = np.abs(1 - np.abs(((diff[d, reactant] / dt[d]) / (vmax / 2))))
        gaps.append(gap)
    min_gap = np.argmin(gaps)
    Km = predecessors[min_gap][reactant + 1]
    for p in range(len(predecessors)):
        A = predecessors[p][reactant + 1]
        if (A > 0) and (np.prod([diff[p, s] for s in s_involved])):
            v_1 = (A / (A + Km)) * (dt[p] / np.abs(diff[p, reactant]))
            v_4 = (A**4 / (A**4 + Km**4)) * (dt[p] / np.abs(diff[p, reactant]))
        else:
            continue
        V_1.append(v_1), V_4.append(v_4)
    vmax_micha, vmax_hill = np.abs(1 / np.mean(V_1)), np.abs(1 / np.mean(V_4))
    std_micha = np.std(V_1) * vmax_micha
    std_hill = np.std(V_4) * vmax_hill
    return vmax_micha, vmax_hill, Km, std_micha, std_hill


def sparsity(T_r, D, file_provenance, infile_order, gamma=10**(-4)):
    """From the traces used to infer a reaction, check the mean number of present species (Algo 8 in article)."""
    predecessors = trace_finder(T_r, D, file_provenance, infile_order)
    zeros = []
    for p, pred in zip(T_r, predecessors):
        count = 0
        species_max = np.max(D[file_provenance[p]], axis=0)[1:] * gamma
        for s, smax in zip(pred[1:], species_max):
            if s < smax:
                count += 1
        zeros.append(count)
    return np.mean(zeros), zeros


def catalyst_inference(reaction, D, diff, header, file_provenance, infile_order, td, pool, alpha, micha, zero_idx, sp, verbose=False):
    """Compute kinetic parameter for reaction and, if needed, catalyst (Algo 7 in article)."""
    cata = []
    reactants, products = np.where(reaction == -1)[0], np.where(reaction == 1)[0]
    if verbose:
        print('considering reaction {} => {}'.format([header[i + 1] for i in reactants], [header[i + 1] for i in products]))
        print('species used for kinetic inference : {}'.format(header[sp + 1]))
        print('{} transitions displaying the reaction'.format(len(pool)))
    s_notinvolved = [i for i in range(len(reaction)) if reaction[i] == 0]
    # pool of transitions (\mathcal{T} in article)
    T = [i for i in range(len(diff)) if i not in zero_idx]
    predecessors = trace_finder(T, D, file_provenance, infile_order)
    dt = td[T]
    avg_k, std, count = mal_inference(predecessors, dt, diff[T], reaction, sp, cata=[])
    if count == 1:
        return np.inf, 0, cata, 'mal'
    if verbose:
        print('average dt value : {}\nmean = {}\nstd = {}\n'.format(dt.mean(), avg_k, std))
    if (std < alpha):
        return std, np.round(avg_k, 4), cata, 'mal'
    else:
        std_S, avg_k_S = np.ones(diff.shape[1]) * np.inf, np.zeros(diff.shape[1])
        # trying mm/hill kinetics :
        if micha and reactants.size == 1:
            vmax_micha, vmax_hill, km, std_micha, std_hill = hill_inference(predecessors, dt, diff[T], reaction)
            hills = [std_micha, std_hill]
            if verbose:
                print('michaelis case\n average dt value : {}\n vmax = {}\n km = {}\n std = {}\n'.format(dt.mean(), vmax_micha, km, std_micha))
                print('hill 4 case\n average dt value : {}\n vmax = {}\n km = {}\n std = {}\n'.format(dt.mean(), vmax_hill, km, std_hill))
            if min(hills) < alpha:
                if np.argmin(hills) == 0:
                    return min(hills), [vmax_micha, km, 1], cata, 'mm'
                else:
                    return min(hills), [vmax_hill, km, 4], cata, 'hill_4'
        # trying to find a catalyst
        for s in s_notinvolved:
            T_s = [T[i] for i in range(len(predecessors)) if predecessors[i][s + 1] > 0]
            dt_s = td[T_s]
            if verbose:
                print('trying species {}\n'.format(header[s + 1]))
            if len(T_s) < 2:
                continue
            predecessors_S = predecessors = trace_finder(T_s, D, file_provenance, infile_order)
            avg_k_S[s], std_S[s], count = mal_inference(predecessors_S, dt_s, diff[T_s], reaction, sp, cata=[s])
            if count == 1:
                return std, np.round(avg_k, 4), cata, 'mal'
            if verbose:
                print('average dt value : {}\nmean = {}\nstd = {}\n'.format(dt_s.mean(), avg_k_S[s], std_S[s]))
        if np.min(std_S) < alpha:
            if verbose:
                print('{} catalyst\n'.format(header[np.argmin(std_S) + 1]))
            cata = np.argmin(std_S)
            return np.min(std_S), np.round(avg_k_S[np.argmin(std_S)], 4), cata, 'mal'
        else:
            if verbose:
                print('failed to found catalyst\n')
            return std, np.round(avg_k, 4), cata, 'mal'


def expand_reacmat(reactions_matrix, occ):
    """Decomposition of 2-valued rows in stoichiometry matrix into two reactions. (case {A=>B A=>C} and {A=>B C=>B}).

    (Part of algo 3 in article)
    """
    rm, new_occ, count, species = np.zeros([1, reactions_matrix.shape[1]]), [], 0, []
    for reac in reactions_matrix:
        if 2 in reac:
            reac_1, reac_2 = np.zeros(reactions_matrix.shape[1]), np.zeros(reactions_matrix.shape[1])
            p, r = np.where(reac == 2)[0], np.where(reac == -2)[0]
            if len(p) > 1:
                reac_1[p[0]], reac_1[r] = 1, -1
                reac_2[p[1]], reac_2[r] = 1, -1
                species.append(p[0]), species.append(p[1])
            else:
                reac_1[r[0]], reac_1[p] = -1, 1
                reac_2[r[1]], reac_2[p] = -1, 1
                species.append(r[0]), species.append(r[1])
            rm = np.vstack((rm, reac_1, reac_2))
            new_occ.append(occ[count]), new_occ.append(occ[count])
        else:
            rm = np.vstack((rm, reac))
            new_occ.append(occ[count])
            reactants, products = np.where(reac == -1)[0], np.where(reac == 1)[0]
            if not reactants.size:
                species.append(products[0])
            elif not products.size:
                species.append(reactants[0])
            else:
                species.append(products[0])
        count += 1
    rm = rm[1:]
    return rm, new_occ, species


def reaction_inference(diff, delta):
    """From the transitions, infer reactions, set of transitions used for the inference and species to target when computing mass action law parameter.

    (Algo 3 in article)
    """
    reactions_matrix = np.zeros(diff.shape)
    # check there is still non null difference vectors
    for d in range(len(diff)):
        if not diff[d].any():
            continue
        abs_transi = np.abs(diff[d])
        idx_highest_val = np.argmax(abs_transi)
        # select the species that will be linked to the highest one consumed/produced.
        I = np.where(abs_transi[idx_highest_val] <= (1 + delta) * abs_transi)[0]
        # if no species can be matched to highest varying species :
        if I.size <= 1:
            prod, gaps = [], []
            if diff[d][idx_highest_val] < 0:
                other_s = [j for j in range(len(diff[d])) if (j != idx_highest_val) and (diff[d][j] > 0)]
                sgn = -1
            else:
                other_s = [j for j in range(len(diff[d])) if (j != idx_highest_val) and (diff[d][j] < 0)]
                sgn = 1
            for s1, s2 in combinations(other_s, 2):
                gap = np.abs(1 - (- (diff[d][s1] + diff[d][s2])) / diff[d][idx_highest_val])
                if gap < delta:
                    prod.append([s1, s2]), gaps.append(gap)
            if gaps:
                g = np.argmin(gaps)
                # use 2 in the stoichio matrix to distinguish transitions that give two reactions instead of one
                reactions_matrix[d, idx_highest_val], reactions_matrix[d, prod[g]] = sgn * 2, - sgn * 2
            else:
                # both cases failed, species cannot be linked to other species/sum of two species, infer simple degradation/synthesis.
                reactions_matrix[d, idx_highest_val] = sgn
        else:
            r, p = list(I[np.where(diff[d][I] < 0)[0]]), list(I[np.where(diff[d][I] > 0)[0]])
            reactions_matrix[d, r], reactions_matrix[d, p] = -1, 1
    reactions_matrix, indexes, inverse = np.unique(reactions_matrix, return_index=True, return_inverse=True, axis=0)
    # indexes where each reaction was witnessed
    empty_reacs = np.where(~reactions_matrix.any(axis=1))[0]
    reacs = [j for j in range(len(reactions_matrix)) if j not in empty_reacs]
    reactions_matrix = reactions_matrix[reacs]
    occurrences = [[j for j, x in enumerate(inverse) if x == l] for l in reacs]
    reactions_matrix, occurrences, species = expand_reacmat(reactions_matrix, occurrences)
    return reactions_matrix, occurrences, species


def reaction_learning_loop(D, diff, delta, file_provenance, infile_order, header, dt, alpha, tr, micha=0, verbose=False):
    """Learn CRN (Algo 1 in article)."""
    new_diff = copy.deepcopy(diff)
    new_diff, zero_idx = filter_var(new_diff, file_provenance, dt, tr)
    best_score, glob_scores = 0, []
    final_cata, final_kinetics, final_matrix = [], [], np.zeros([1, new_diff.shape[1]])
    predecessors = trace_finder(range(len(new_diff)), D, file_provenance, infile_order)
    while best_score < alpha and new_diff.any():
        reactions_matrix, occurrences, species = reaction_inference(new_diff, delta)
        score, glob_kinetics, catalysts = [], [], []
        len_occ, keep = [], [i for i in range(len(reactions_matrix))]
        # keep only the most present reactions
        for reac, occ in zip(reactions_matrix, occurrences):
            len_occ.append(len(occ))
            keep = np.argsort(len_occ)[-4:]
        kine_types = []
        for reac in keep:
            std, kinetics, cata, kine_type = catalyst_inference(reactions_matrix[reac], D, new_diff, header, file_provenance, infile_order, dt, occurrences[reac], alpha, micha, zero_idx, species[reac], verbose)
            mean_zeros, zeros = sparsity(occurrences[reac], D, file_provenance, infile_order)
            kine_types.append(kine_type)
            if verbose:
                print('zeros found for transitions considered : {}. penalized score {}\n'.format(zeros, std / (1 + mean_zeros)))
            score.append(std / (1 + mean_zeros)), glob_kinetics.append(kinetics), catalysts.append(cata)
        best = np.nanargmin(score)
        kine, best_score = kine_types[best], score[best]
        print(score)
        if best_score > alpha:
            break
        fiable_reac, fiable_kinetics = reactions_matrix[keep[best]], glob_kinetics[best]
        final_matrix = np.vstack([final_matrix, fiable_reac])
        glob_scores.append(best_score), final_cata.append(catalysts[best]), final_kinetics.append(fiable_kinetics)
        reactants, products = list(np.where(fiable_reac == -1)[0]), list(np.where(fiable_reac == 1)[0])
        print('best reac is {}({}) for {} => {}  with score {}\n \n'.format(kine, fiable_kinetics, [header[i + 1] for i in reactants], [header[i + 1] for i in products], score[best]))
        new_diff, old_idx = transition_update(new_diff, fiable_reac, fiable_kinetics, catalysts[best], predecessors, dt, zero_idx, file_provenance, tr, kine)
        # prevent inferring infinitely A=>B B=>A
        if len(final_matrix > 3):
            deb, inv = np.unique(final_matrix, return_inverse=True, axis=0)
            co = np.bincount(inv)
            if (co > 2).any():
                break
            else:
                continue
    final_matrix = np.delete(final_matrix, 0, axis=0)
    # return only one version of a reaction : the first one inferred
    if final_matrix.size > 0:
        final_matrix, idx = np.unique(final_matrix, return_index=True, axis=0)
        final_cata, final_kinetics, glob_scores = [final_cata[i] for i in idx], [final_kinetics[i] for i in idx], [glob_scores[i] for i in idx]
    return new_diff, final_matrix, final_cata, final_kinetics, glob_scores


def transition_update(diff, reac, kinetics, cata, predecessors, dt, zero_idx, fp, tr, kine_type):
    """Update transitions given a reaction and kinetic parameters (Algo 9 in article)."""
    reactants, products = list(np.where(reac == -1)[0]), list(np.where(reac == 1)[0])
    acting_species = reactants.copy()
    # do not update transitions where reactants are absent
    modif_idx = [i for i in range(len(predecessors)) if (np.prod([predecessors[i][r + 1] for r in reactants]) > 0) and i not in zero_idx]
    if type(cata) is not list:
        acting_species += [cata]
        modif_idx = [i for i in modif_idx if predecessors[i][cata + 1]]
    for i in modif_idx:
        if kine_type == 'mal':
            var_update = dt[i] * np.prod([predecessors[i][a + 1] for a in acting_species]) * kinetics
        else:
            var_update = dt[i] * kinetics[0] * (predecessors[i][reactants[0] + 1] / (kinetics[1] + predecessors[i][reactants[0] + 1]))
        diff[i, products] -= var_update
        diff[i, reactants] += var_update
    diff, zero_idx = filter_var(diff, fp, dt, tr)
    return diff, zero_idx
