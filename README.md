Code related to the article "A Statistical Learning Algorithm for Inferring Reaction Systems from Data Time Series", submitted to CMSB 2019.

The algorithm is written in Python. Please make sure the following dependancies are installed on your computer :

- Numpy
- Matplotlib

inference.py and data_processing.py contain the algorithms. model_evaluation.py contains the functions necessary to evaluate the algorithm on synthetic data. test.txt is an empty  file that is going to be filled with the path of the csv files used by the algorithm.

The code can be run with the command "python3 main.py" in shell. This will trigger the evaluation for each of the four models described in the article. The output is made of several pdf files including all plots present in the article. The time of computation is around 7 minutes on a standard laptop.

Notice that the blue curves plotted are obtained with a run of the algorithm based on several files. These files are randomly chosen among the pool of files that can be found in the folders (e.g 5 files among 150). Each of these files are numerical simulation of a model with random initial conditions. Hence why from one run to another, blue curves may differ.
