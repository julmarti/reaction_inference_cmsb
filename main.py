import sys

from model_evaluation import eval_alpha_cano, eval_sub_cano, eval_time_cano, evaluate_alpha, evaluate_init, evaluate_sub, evaluate_time, plot_generator, truth_model

import numpy as np


# settings for sensibility analysis

list_init = [1, 2, 3, 4, 5, 7, 10, 15]
list_sub = np.linspace(0, 10, 11)
list_alpha = [0.05, 0.1, 0.2, 0.3, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2]
list_time = [0.01, 0.1, 0.5, 1, 5, 10, 15]
# gamma is already hardcoded in function sparsity()
beta, alpha, delta = .03, .3, 1
nfiles, sub = 5, 0

### SIMPLE PARALLEL CASE ###

score_ini, score_alpha, score_time, score_sub = [], [], [], []
score_sub_one, score_alpha_one, score_time_one = [], [], []

# run sensibility analysis for canonical trace

true_cata, true_matrix, discarded = truth_model('parallel')
path = 'simple_parallel/cano_parallel'

score = eval_time_cano(path, list_time, sub, true_matrix, true_cata, delta, alpha, beta)
score_time_one.append(score)
score = eval_sub_cano(path, 10, list_sub, true_matrix, true_cata, delta, alpha, beta)
score_sub_one.append(score)
score = eval_alpha_cano(path, 10, sub, true_matrix, true_cata, delta, list_alpha, beta)
score_alpha_one.append(score)

# run sensibility analysis for multiple traces

path = 'simple_parallel/simple_parallel'

for rep in range(10):
    score = evaluate_init(path, 1, np.diff(list_init), sub, true_matrix, true_cata, delta, alpha, beta, discarded)
    score_ini.append(score)
    score = evaluate_time(path, list_time, nfiles, sub, true_matrix, true_cata, delta, alpha, beta, discarded)
    score_time.append(score)
    score = evaluate_sub(path, 10, nfiles, list_sub, true_matrix, true_cata, delta, alpha, beta, discarded)
    score_sub.append(score)
    score = evaluate_alpha(path, 1, nfiles, sub, true_matrix, true_cata, delta, list_alpha, beta, discarded)
    score_alpha.append(score)

mean_ini, mean_time, mean_sub, mean_alpha = np.mean(np.array(score_ini), axis=0), np.mean(np.array(score_time), axis=0), np.mean(np.array(score_sub), axis=0), np.mean(np.array(score_alpha), axis=0)
std_ini, std_time, std_sub, std_alpha = np.std(np.array(score_ini), axis=0), np.std(np.array(score_time), axis=0), np.std(np.array(score_sub), axis=0), np.std(np.array(score_alpha), axis=0)

plot_generator(list_alpha, list_sub, list_time, list_init, mean_alpha, mean_sub,
               mean_time, mean_ini, std_alpha, std_sub, std_time, std_ini,
               score_sub_one, score_time_one, score_alpha_one, 'parallel_sensibility')

### PRODUCT PARALLEL CASE ###

path = 'product_parallel/cano_prod'

score_ini, score_alpha, score_time, score_sub = [], [], [], []
score_sub_one, score_alpha_one, score_time_one = [], [], []
true_cata, true_matrix, discarded = truth_model('product_parallel')

# run sensibility analysis for canonical trace

score = eval_time_cano(path, list_time, sub, true_matrix, true_cata, delta, alpha, beta)
score_time_one.append(score)
score = eval_sub_cano(path, 10, list_sub, true_matrix, true_cata, delta, alpha, beta)
score_sub_one.append(score)
score = eval_alpha_cano(path, 10, sub, true_matrix, true_cata, delta, list_alpha, beta)
score_alpha_one.append(score)

# run sensibility analysis for multiple traces

path = 'product_parallel/product_parallel'

for rep in range(10):
    score = evaluate_init(path, 1, np.diff(list_init), sub, true_matrix, true_cata, delta, alpha, beta, discarded)
    score_ini.append(score)
    score = evaluate_time(path, list_time, nfiles, sub, true_matrix, true_cata, delta, alpha, beta, discarded)
    score_time.append(score)
    score = evaluate_sub(path, 10, nfiles, list_sub, true_matrix, true_cata, delta, alpha, beta, discarded)
    score_sub.append(score)
    score = evaluate_alpha(path, 1, nfiles, sub, true_matrix, true_cata, delta, list_alpha, beta, discarded)
    score_alpha.append(score)

mean_ini, mean_time, mean_sub, mean_alpha = np.mean(np.array(score_ini), axis=0), np.mean(np.array(score_time), axis=0), np.mean(np.array(score_sub), axis=0), np.mean(np.array(score_alpha), axis=0)
std_ini, std_time, std_sub, std_alpha = np.std(np.array(score_ini), axis=0), np.std(np.array(score_time), axis=0), np.std(np.array(score_sub), axis=0), np.std(np.array(score_alpha), axis=0)

plot_generator(list_alpha, list_sub, list_time, list_init, mean_alpha, mean_sub,
               mean_time, mean_ini, std_alpha, std_sub, std_time, std_ini,
               score_sub_one, score_time_one, score_alpha_one, 'product_parallel_sensibility')

### REACTANT PARALLEL CASE ###

path = 'reactant_parallel/cano_reac'
score_ini, score_alpha, score_time, score_sub = [], [], [], []
score_sub_one, score_alpha_one, score_time_one = [], [], []
true_cata, true_matrix, discarded = truth_model('reactant_parallel')

# run sensibility analysis for canonical trace

score = eval_time_cano(path, list_time, sub, true_matrix, true_cata, .3, alpha, beta)
score_time_one.append(score)
score = eval_sub_cano(path, 10, list_sub, true_matrix, true_cata, .3, alpha, beta)
score_sub_one.append(score)
score = eval_alpha_cano(path, 10, sub, true_matrix, true_cata, .3, list_alpha, beta)
score_alpha_one.append(score)

# run sensibility analysis for multiple traces
path = 'reactant_parallel/reactant_parallel'
for rep in range(10):
    score = evaluate_init(path, 1, np.diff(list_init), sub, true_matrix, true_cata, delta, alpha, beta, discarded)
    score_ini.append(score)
    score = evaluate_time(path, list_time, nfiles, sub, true_matrix, true_cata, delta, alpha, beta, discarded)
    score_time.append(score)
    score = evaluate_sub(path, 10, nfiles, list_sub, true_matrix, true_cata, delta, alpha, beta, discarded)
    score_sub.append(score)
    score = evaluate_alpha(path, 1, nfiles, sub, true_matrix, true_cata, delta, list_alpha, beta, discarded)
    score_alpha.append(score)

mean_ini, mean_time, mean_sub, mean_alpha = np.mean(np.array(score_ini), axis=0), np.mean(np.array(score_time), axis=0), np.mean(np.array(score_sub), axis=0), np.mean(np.array(score_alpha), axis=0)
std_ini, std_time, std_sub, std_alpha = np.std(np.array(score_ini), axis=0), np.std(np.array(score_time), axis=0), np.std(np.array(score_sub), axis=0), np.std(np.array(score_alpha), axis=0)

plot_generator(list_alpha, list_sub, list_time, list_init, mean_alpha, mean_sub,
               mean_time, mean_ini, std_alpha, std_sub, std_time, std_ini,
               score_sub_one, score_time_one, score_alpha_one, 'reactant_parallel_sensibility')

### CHAIN CASE ###

path = 'chain/cano_chain'

score_ini, score_alpha, score_time, score_sub = [], [], [], []
score_sub_one, score_alpha_one, score_time_one = [], [], []
true_cata, true_matrix, discarded = truth_model('chain')

# run sensibility analysis for canonical trace

score = eval_time_cano(path, list_time, sub, true_matrix, true_cata, 1, alpha, beta)
score_time_one.append(score)
score = eval_sub_cano(path, 10, list_sub, true_matrix, true_cata, delta, alpha, beta)
score_sub_one.append(score)
score = eval_alpha_cano(path, 10, sub, true_matrix, true_cata, delta, list_alpha, beta)
score_alpha_one.append(score)

# run sensibility analysis for multiple traces

path = 'chain/chain'
for rep in range(10):
    score = evaluate_init(path, 10, np.diff(list_init), sub, true_matrix, true_cata, delta, alpha, beta, discarded)
    score_ini.append(score)
    score = evaluate_time(path, list_time, nfiles, sub, true_matrix, true_cata, delta, alpha, beta, discarded)
    score_time.append(score)
    score = evaluate_sub(path, 10, nfiles, list_sub, true_matrix, true_cata, delta, alpha, beta, discarded)
    score_sub.append(score)
    score = evaluate_alpha(path, 10, nfiles, sub, true_matrix, true_cata, delta, list_alpha, beta, discarded)
    score_alpha.append(score)

mean_ini, mean_time, mean_sub, mean_alpha = np.mean(np.array(score_ini), axis=0), np.mean(np.array(score_time), axis=0), np.mean(np.array(score_sub), axis=0), np.mean(np.array(score_alpha), axis=0)
std_ini, std_time, std_sub, std_alpha = np.std(np.array(score_ini), axis=0), np.std(np.array(score_time), axis=0), np.std(np.array(score_sub), axis=0), np.std(np.array(score_alpha), axis=0)

plot_generator(list_alpha, list_sub, list_time, list_init, mean_alpha, mean_sub,
               mean_time, mean_ini, std_alpha, std_sub, std_time, std_ini,
               score_sub_one, score_time_one, score_alpha_one, 'chain_sensibility')

sys.exit()
