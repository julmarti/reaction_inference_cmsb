import copy
import csv
from collections import Counter
from typing import List, Tuple

import numpy as np

Header = List[str]
Point = List[float]
Series = List[Point]
Data = List[Series]


def read_multiple_data(files_list: str, T=np.inf) -> Tuple[Header, Data, List]:
    """Read a list of biocham simulation data files from CSV files and return it."""
    header: Header = []
    data: Data = []
    discarded, idx_discarded = 0, []
    with open(files_list) as f:
        files = [line.rstrip('\n') for line in f]
    for file in files:
        time_series: Series = []
        header = []
        with open(file, "r", newline="") as f:
            for row in csv.reader(f):
                if header == []:
                    # remove first '#' character
                    row[0] = row[0][1:]
                    header = row
                else:
                    time_series.append(list(map(float, row)))
        # discard traces with no species at the beginning as well as no synthesis
        if ((sum(time_series[0][1:]) == sum(time_series[1][1:])) and (sum(time_series[0][1:]) == 0)):
            discarded += 1
            idx_discarded.append(file)
            continue
        # discard traces with several time points where t = 0 (numerical issues)
        if (time_series[1][0] == 0):
            discarded += 1
            idx_discarded.append(file)
            continue
        if T >= time_series[-1][0]:
            data.append(time_series)
        else:
            for t in range(len(time_series)):
                if time_series[t][0] > T:
                    data.append(time_series[0:t])
                    break
    return header, data, idx_discarded


def subsampling(data: Data, step: float) -> Data:
    """Take list of lists of biocham csv files and return subsampled data."""
    sub_data = []
    if not step:
        return data
    for trace in range(len(data)):
        steps = [x * step for x in range(np.int((1 / step) * data[trace][-1][0]) + 1) if x * step < data[trace][-1][0] + 0.01]
        idx_sub = np.unique(np.searchsorted([data[trace][i][0] for i in range(0, len(data[trace]) - 1)], steps, side="left"))
        sub_data.append([data[trace][i] for i in idx_sub])
    return sub_data


def get_differences(data: Data) -> Tuple[Series, list, list]:
    """Compute finite differences and associate file index and infile index provenance to the difference."""
    diff = np.diff(data[0], axis=0)
    file_provenance = [0 for i in range(len(diff))]
    for l in range(1, len(data)):
        d = np.diff(data[l], axis=0)
        diff = np.concatenate([diff, d], axis=0)
        file_provenance += [l for i in range(len(d))]
    occurences = Counter(file_provenance)
    infile_order = []
    for keys in occurences.keys():
        for i in range(occurences[keys]):
            infile_order.append(i)
    return diff, file_provenance, infile_order


def filter_var(diff, fp, dt, tr) -> Tuple[Series, list]:
    """Remove insignificant variations (Algo 2 in article)."""
    diff_dt = np.abs(copy.deepcopy(diff))
    for i in range(len(diff_dt)):
        diff_dt[i] = diff_dt[i] / dt[i]
    for d in range(len(diff)):
        n_file = fp[d]
        others = [i for i in range(len(fp)) if fp[i] == n_file]
        ma = np.max(diff_dt[others], axis=0)
        for s in range(diff_dt.shape[1]):
            if (diff[d, s] != 0) and ((diff_dt[d, s]) < tr * ma[s]):
                diff[d, s] = 0
    zero_idx = [i for i in range(len(diff)) if i in np.where((diff == np.zeros(diff.shape[1])).all(axis=1))[0]]
    return diff, zero_idx


def trace_finder(idx_diff, data, file_provenance, infile_order) -> Data:
    """Return predecessor states associated to a list of difference vectors indexes."""
    list_pred = []
    for i in idx_diff:
        file_number = file_provenance[i]
        order = infile_order[i]
        pred = data[file_number][order]
        list_pred.append(pred)
    return list_pred
