import copy

from data_processing import get_differences, read_multiple_data, subsampling

from inference import reaction_learning_loop

import matplotlib.pyplot as plt
from matplotlib import rc

import numpy as np

rc('text', usetex=True)


def f1_score(reac_truth, reac_inferred, cata_truth, cata_inferred):
    """Compute F1-Score."""
    TP, FP, FN = 0, 0, 0
    TP_idx = []
    if reac_inferred.size == 0:
        return 0
    for i in range(len(reac_inferred)):
        # check if reac was inferred
        idx_reac = np.where((reac_inferred[i] == reac_truth).all(axis=1))[0]
        if idx_reac.size:
            # check if cata match
            if cata_inferred[i] == cata_truth[idx_reac[0]]:
                TP += 1
                TP_idx.append(idx_reac[0])
            else:
                FP += 1
        else:
            FP += 1
    FN_idx = [i for i in range(len(reac_truth)) if i not in TP_idx]
    FN = len(FN_idx)
    precision = TP / (TP + FP)
    recall = TP / (TP + FN)
    # handle division by zero case
    return 0 if precision + recall == 0 else 2 * (precision * recall) / (precision + recall)


def evaluate_sub(path, T, n_files, sub_list, reac_truth, cata_truth, delta, alpha, tr, discarded):
    """Evaluate subsampling. n_files traces are chosen randomly among the pool of traces and a subsampling step applied.

    Algorithm is then run and the F1_score computed. This is done for the same files, but with different subsampling steps.
    """
    score = []
    pool = [i for i in range(1, 150) if i not in discarded]
    files = np.random.choice(pool, n_files, replace=False)
    with open('test.txt', 'w') as f:
        for i in files:
            print('{}{}.csv'.format(path, i), file=f)
    header, data, discarded = read_multiple_data('test.txt', T)
    new_sub_list = np.copy(sub_list)
    new_sub_list /= T
    for s in new_sub_list:
        new_data = copy.deepcopy(data)
        print('with files {}, T={}, sub={}, alpha={}'.format(files, T, s, alpha))
        new_data = subsampling(new_data, s)
        diff, fp, ifp = get_differences(new_data)
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(new_data, diff[:, 1:], delta, fp, ifp, header, diff[:, 0], alpha, tr, micha=1, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def evaluate_init(path, T, list_n_files, sub, reac_truth, cata_truth, delta, alpha, tr, discarded):
    """Evaluate initial conditions. The algorithm is run with a trace (X_1) as input, then with traces (X_1, X_2) etc."""
    score = []
    pool = [i for i in range(1, 150) if i not in discarded]
    files = np.random.choice(pool, 1, replace=False)
    for n in range(len(list_n_files) + 1):
        with open('test.txt', 'w') as f:
            for i in files:
                print('{}{}.csv'.format(path, i), file=f)
        header, data, discarded = read_multiple_data('test.txt', T)
        print('with files {}, T={}, sub={}, alpha={}'.format(files, T, sub, alpha))
        diff, fp, ifp = get_differences(data)
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, diff[:, 1:], delta, fp, ifp, header, diff[:, 0], alpha, tr, micha=1, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
        pool = [i for i in pool if i not in files]
        if n == len(list_n_files):
            return score
        files = np.append(files, np.random.choice(pool, list_n_files[n], replace=False))


def evaluate_time(path, list_T, n_files, sub, reac_truth, cata_truth, delta, alpha, tr, discarded):
    """Evaluate Time horizon. At each iteration the algorithm is applied on the same files but with an increasing time horizon."""
    score = []
    pool = [i for i in range(1, 150) if i not in discarded]
    files = np.random.choice(pool, n_files, replace=False)
    with open('test.txt', 'w') as f:
        for i in files:
            print('{}{}.csv'.format(path, i), file=f)
    for T in list_T:
        print('with files {}, T={}, sub={}, alpha={}'.format(files, T, sub, alpha))
        header, data, discarded = read_multiple_data('test.txt', T)
        diff, fp, ifp = get_differences(data)
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, diff[:, 1:], delta, fp, ifp, header, diff[:, 0], alpha, tr, micha=1, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def evaluate_alpha(path, T, n_files, sub, reac_truth, cata_truth, delta, list_alpha, tr, discarded):
    """Evaluate Alpha sensibility. At each iteration the algorithm is applied on the same files but with an increasing reaction acceptance threshold alpha."""
    score = []
    pool = [i for i in range(1, 150) if i not in discarded]
    files = np.random.choice(pool, n_files, replace=False)
    with open('test.txt', 'w') as f:
        for i in files:
            print('{}{}.csv'.format(path, i), file=f)
    header, data, discarded = read_multiple_data('test.txt', T)
    diff, fp, ifp = get_differences(data)
    for alpha in list_alpha:
        print('with files {}, T={}, sub={}, alpha={}'.format(files, T, sub, alpha))
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, diff[:, 1:], delta, fp, ifp, header, diff[:, 0], alpha, tr, micha=1, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def eval_sub_cano(file_path, T, sub_list, reac_truth, cata_truth, delta, alpha, tr):
    """Evaluate subsampling for canonical trace."""
    score = []
    with open('test.txt', 'w') as f:
        print('{}.csv'.format(file_path), file=f)
    header, data, discarded = read_multiple_data('test.txt', T)
    new_sub_list = np.copy(sub_list)
    new_sub_list /= T
    for s in new_sub_list:
        new_data = copy.deepcopy(data)
        new_data = subsampling(new_data, s)
        diff, fp, ifp = get_differences(new_data)
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(new_data, diff[:, 1:], delta, fp, ifp, header, diff[:, 0], alpha, tr, micha=1, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def eval_time_cano(file_path, list_T, sub, reac_truth, cata_truth, delta, alpha, tr):
    """Evaluate time horizon for canonical trace."""
    score = []
    with open('test.txt', 'w') as f:
        print('{}.csv'.format(file_path), file=f)
    for T in list_T:
        header, data, discarded = read_multiple_data('test.txt', T)
        diff, fp, ifp = get_differences(data)
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, diff[:, 1:], delta, fp, ifp, header, diff[:, 0], alpha, tr, micha=1, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def eval_alpha_cano(file_path, T, sub, reac_truth, cata_truth, delta, list_alpha, tr):
    """Evaluate alpha for canonical trace."""
    score = []
    with open('test.txt', 'w') as f:
        print('{}.csv'.format(file_path), file=f)
    header, data, discarded = read_multiple_data('test.txt', T)
    diff, fp, ifp = get_differences(data)
    for alpha in list_alpha:
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, diff[:, 1:], delta, fp, ifp, header, diff[:, 0], alpha, tr, micha=1, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def truth_model(model):
    """Return model stoichioemetry matrix, catalysts and traces containing simulation errors for selected model.

    The traces containing simulation errors will therefore be ignored in the random sampling of traces in the evalution process.
    """
    if model == 'parallel':
        true_cata = [[2], [], []]
        true_matrix = np.zeros([3, 6])
        true_matrix[0] = [0, 0, 0, 0, 1, -1]
        true_matrix[1] = [0, 0, 1, -1, 0, 0]
        true_matrix[2] = [1, -1, 0, 0, 0, 0]
        d = [2, 31, 58, 68, 81, 95, 97, 105, 111, 148]
        return true_cata, true_matrix, d

    if model == 'product_parallel':
        true_cata = [[1], [], []]
        true_matrix = np.zeros([3, 5])
        true_matrix[0] = [0, 0, 0, 1, -1]
        true_matrix[1] = [0, -1, 1, 0, 0]
        true_matrix[2] = [1, -1, 0, 0, 0]
        d = [75, 145]
        return true_cata, true_matrix, d

    if model == 'reactant_parallel':
        true_cata = [[0], [], []]
        true_matrix = np.zeros([3, 5])
        true_matrix[0] = [0, 0, 0, 1, -1]
        true_matrix[1] = [1, -1, 0, 0, 0]
        true_matrix[2] = [1, 0, -1, 0, 0]
        d = [54, 70, 74, 91, 119, 138]
        return true_cata, true_matrix, d

    if model == 'chain':
        true_cata = [[], [], [], []]
        true_matrix = np.zeros([4, 5])
        true_matrix[0] = [0, 0, 0, 1, -1]
        true_matrix[1] = [0, 0, 1, -1, 0]
        true_matrix[2] = [0, 1, -1, 0, 0]
        true_matrix[3] = [1, -1, 0, 0, 0]
        d = [42, 52, 67, 81, 89, 142]
        return true_cata, true_matrix, d


def plot_generator(list_alpha, list_sub, list_time, list_init, mean_alpha, mean_sub,
                   mean_time, mean_ini, std_alpha, std_sub, std_time, std_ini,
                   score_sub_one, score_time_one, score_alpha_one, output):
    """Save results in pdf file named "{output}.pdf"."""
    fig, axes = plt.subplots(2, 2, figsize=(15, 10))
    sinit = [1 for i in range(len(list_init))]
    fig.text(0.07, 0.5, r'$F_1$ Score', ha='center', va='center', rotation='vertical')
    axes[0, 0].set_xlabel('timelapse as \% of the time horizon')
    axes[1, 0].set_xlabel('time horizon')
    axes[0, 1].set_xlabel('number of traces included')
    axes[0, 1].set_xlim(0, 16)
    axes[1, 1].set_xlabel(r'reaction acceptance threshold $\alpha$')
    [axes[0, i].set_ylim(0, 1.1) for i in [0, 1]]
    [axes[1, i].set_ylim(0, 1.1) for i in [0, 1]]
    axes[0, 0].errorbar(list_sub, mean_sub, yerr=std_sub, marker='o', linestyle='--')
    axes[0, 0].plot(list_sub, score_sub_one[0], marker='o', linestyle='--', color='r')
    axes[0, 1].errorbar(list_init, mean_ini, yerr=std_ini, marker='o', linestyle='--')
    axes[0, 1].plot(list_init, sinit, marker='o', linestyle='--', color='r')
    axes[1, 0].errorbar(list_time, mean_time, yerr=std_time, marker='o', linestyle='--')
    axes[1, 0].plot(list_time, score_time_one[0], marker='o', linestyle='--', color='r')
    axes[1, 1].errorbar(list_alpha, mean_alpha, yerr=std_alpha, marker='o', linestyle='--')
    axes[1, 1].plot(list_alpha, score_alpha_one[0], marker='o', linestyle='--', color='r')
    fig.savefig('{}.pdf'.format(output), transparent=True)
